var gulp = require('gulp'),
    sass = require('gulp-sass'),
    csscomb = require('gulp-csscomb'),
    cssbeautify = require('gulp-cssbeautify'),
    autoprefixer = require('gulp-autoprefixer'),
    livereload = require('gulp-livereload'),
    pixrem = require('gulp-pxtorem'),
    uglify = require('gulp-uglify'),
    minifycss = require('gulp-minify-css'),
    gulpif = require('gulp-if'),
    concatJs = require('gulp-concat'),
    concatCss = require('gulp-concat-css'),
    rename = require('gulp-rename'),
    cache = require('gulp-cache'),
    imagemin = require('gulp-imagemin');

livereload.listen();
/* compile scss files */
gulp.task('sass', function(){
  return gulp.src('custom-sass/**/*.scss')
  .pipe(sass())
  .pipe(pixrem())
  .pipe(csscomb())
  .pipe(cssbeautify({
    indent: '  ',
    openbrace: 'separate-line',
    autosemicolon: true
  }))
  .pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
	}))
  .pipe(livereload())
  .pipe(gulp.dest('css'))
});

/* concat css files */
gulp.task('concat-css', function(){
  return gulp.src('css/*.css')
  .pipe(concatCss('concat/bundle.css'))
  .pipe(gulp.dest('css/'))
});

/* minify css files */
gulp.task('minify-css', function(){
  return gulp.src('css/concat/bundle.css')
  .pipe(minifycss({
      compatibility: 'ie8'
  }))
  .pipe(rename({
    suffix: '.min'
  }))
  .pipe(gulp.dest('css/mini'))
});

/* concat js files */
gulp.task('concat-js', function(){
  return gulp.src('js/*.js')
  .pipe(concatJs('concat/bundle.js'))
  .pipe(gulp.dest('js'))
});

/* minify js files */
gulp.task('minify-js', function(){
  return gulp.src('js/concat/bundle.js')
  .pipe(uglify())
  .pipe(rename({
    suffix: '.min'
  }))
  .pipe(gulp.dest('js/mini'))
});


// Optimized images
gulp.task('images', function(){
  return gulp.src('images/*.+(png|jpg|jpeg|gif|svg)')
  .pipe(cache(imagemin({
    interlaced: true
  })))
  .pipe(gulp.dest('images/build-images'))
});
// Task automate
gulp.task('watch', function(){
  gulp.watch('custom-sass/**/*.scss', ['sass']);
})
